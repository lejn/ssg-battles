extends Node2D

const MAX_LENGTH = 2000

var target = null

onready var beam = $Beam
onready var end = $End
onready var rayCast2D = $RayCast2D

func start(_transform, _target):
	global_transform = _transform
	target = _target  

func _physics_process(delta):
	if !target:
		return
	
#	var mouse_position = get_local_mouse_position()
#	var max_cast_to_old = mouse_position.normalized() * MAX_LENGTH
#	var max_cast_to = target.normalized() * MAX_LENGTH
	rayCast2D.cast_to = target
	end.global_position = target
	beam.rotation = (target - position).angle()-rotation
	beam.region_rect.end.x = (target-position).length()*2
#	if rayCast2D.is_colliding():
#	end.global_position = rayCast2D.get_collision_point()
#	else:
#		end.global_position = rayCast2D.cast_to
	
#	beam.rotation = rayCast2D.cast_to.angle()
#	beam.region_rect.end.x = end.position.length()
	
func _on_Lifetime_timeout():
	queue_free()
