extends KinematicBody2D

var speed = 200
var target = null
var velocity = Vector2.ZERO
export (PackedScene) var WeaponClass

enum Status{
	UNSELECTED,
	SELECTED,
	ATTACKING
}

onready var status = Status.UNSELECTED


func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed and status == Status.ATTACKING:
			shoot(event.position)
		if event.button_index == BUTTON_RIGHT and event.pressed and status == Status.SELECTED:
			target = event.position
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_A:
			print("Attack mode")
			status = Status.ATTACKING
		if event.scancode == KEY_U:
			print("Ship unselected")
			status = Status.UNSELECTED
		if event.scancode == KEY_S:
			print("Ship selected")
			status = Status.SELECTED
			
func _physics_process(_delta):
	if target:
		look_at(target)
		velocity = transform.x * speed
		# stop moving if we get close to the target
		if position.distance_to(target) > 5:
			velocity = move_and_slide(velocity)

func shoot(t):
	var b = WeaponClass.instance()
	owner.add_child(b)
	b.start($Muzzle.global_transform, t)


func _on_Ship_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			status = Status.SELECTED
			print('Ship SELECTED by clicking')
#		if event.button_index == BUTTON_RIGHT and event.pressed:
#			target = event.position
